// Empty constructor
function NFC() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
NFC.prototype.write = function(successCallback, errorCallback, options) {
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    successCallback(json);
  }, errorCallback, 'NFCPlugin', 'write', [options]);
}

NFC.prototype.initPort = function(options) {
  return (new Promise(function(resolve,reject){
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    resolve(json);
  }, function(err) {
    reject(err);
  }, 'NFCPlugin', 'initPort', [options]);}));
}

NFC.prototype.ledOn = function(options) {
  return (new Promise(function(resolve,reject){
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    resolve(json);
  }, function(err) {
    reject(err);
  }, 'NFCPlugin', 'ledOn', [options]);}));
}

NFC.prototype.ledOff = function() {
  var options = {};
  return (new Promise(function(resolve,reject){
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    resolve(json);
  }, function(err) {
    reject(err);
  }, 'NFCPlugin', 'ledOff', [options]);}));
}

NFC.prototype.cancel = function() {
  var options = {};
  return (new Promise(function(resolve,reject){
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    resolve(json);
  }, function(err) {
    reject(err);
  }, 'NFCPlugin', 'cancel', [options]);}));
}

NFC.prototype.read = function(successCallback, errorCallback, options) {
  cordova.exec(function(str) {
    var json = JSON.parse(str);
    successCallback(json);
  }, errorCallback, 'NFCPlugin', 'read', [options]);
}

// Installation constructor that binds NFCPlugin to window
NFC.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.NFC = new NFC();
  return window.plugins.NFC;
};
cordova.addConstructor(NFC.install);
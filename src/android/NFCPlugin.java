package com.appivo.cordova.nfc;

import java.util.Map;

import org.apache.cordova.*;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.serialport.Application;
import android.serialport.SerialPort;
import android.app.smdt.SmdtManager;


public class NFCPlugin extends CordovaPlugin {
    
    private final static String CMD_CANCEL = "cancel";
	private final static String CMD_WRITE = "write";
	private final static String CMD_READ = "read";
    private final static String CMD_TURNON  = "ledOn";
    private final static String CMD_TURNOFF   = "ledOff";
	private final static String CMD_INITPORT = "initPort";
	
	private static String ID="";
	private static boolean ReadOnceID=false,AutoRead=false;
	private static boolean StopListening=false;
	private static int block=5;
	private static String portName = "";
	private static String blockData = "";
	
	private static JSONArray newID;
	private static boolean writeID=false,readID=false;    // 'true' to write/read
	
	private static boolean R=false,G=false,B=false;

    private CallbackContext callback;
	
    private SmdtManager smdtManager;
	
	protected Application mApplication;
	protected SerialPort mSerialPort;
	private InputStream mInputStream = null;
	private OutputStream mOutputStream = null;
	private boolean threadFlag = true;
	private byte data[] = new byte[64];
	private int count = 0;
	private int size = 0;
	
	private String currentCmd,et_send,tv_bcc,et_stx="20 00";

	private boolean stxCheck = false;
	
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				lengthChecker();
				break;
			}
		}
	};


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
		
		smdtManager = SmdtManager.create(getActivity().getBaseContext());
		
		ReadThread();
    }

	public void initPort(CallbackContext callbackContext) {
        PluginResult result1 = new PluginResult(PluginResult.Status.NO_RESULT);
        result1.setKeepCallback(true);
        callbackContext.sendPluginResult(result1);              
        callback = callbackContext;
	
	    if (mInputStream == null) {
			mApplication = (Application) getActivity().getApplication();
			try {
				mSerialPort = mApplication.getSerialPort(portName);
				mOutputStream = mSerialPort.getOutputStream();
				
				mInputStream = mSerialPort.getInputStream();
			} catch (IOException e) {
				if (callback != null) {
					PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
					result.setKeepCallback(true);
					callback.sendPluginResult(result);
				}
				e.printStackTrace();
			}
		}
	
		sendSuccessMsg();
	}

	private void ReadThread() {
		new Thread(new Runnable() {
			public void run() {
				while (threadFlag) {
					if (mInputStream != null) {
						try {
							byte[] receiveBuffer = new byte[64];
							size = mInputStream.read(receiveBuffer);

							System.arraycopy(receiveBuffer, 0, data, count, size);

							count = size + count;
							stxChecker();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}).start();
	}

	public static String bytes2HexString(byte[] b, int size) {
		String hex = "";
		String out = "";
		for (int i = 0; i < size; i++) {
			hex = Integer.toHexString(b[i] & 0xFF);

			if (hex != null) {
				if (hex.length() == 1) {
					hex = '0' + hex;
				}
				out += hex + " ";
			}

		}

		return out.toUpperCase();
	}
	
	public static String bytes2HexStringNoSpace(byte[] b, int size) {
		String hex = "";
		String out = "";
		for (int i = 0; i < size; i++) {
			hex = Integer.toHexString(b[i] & 0xFF);

			if (hex != null) {
				if (hex.length() == 1) {
					hex = '0' + hex;
				}
				out += hex;
			}

		}

		return out.toUpperCase();
	}
	
	public static String bytes2String(byte[] b, int size) {
		String out = "";
		for (int i = 0; i < size; i++) {
			 out += (char)b[i];
		}

		return out;
	}
	
	public static String getEmptyHex(int size){
		String out = "";
		for (int i = 0; i < size; i++) {
			out += "30 ";
		}

		return out.toUpperCase();
	}

	public byte[] toArray(String cmd) {
		cmd = cmd.trim().replace(" ", "");
		int cmd_length = cmd.length() / 2;
		byte[] outBuffer = new byte[cmd_length];

		if (cmd_length > 0) {
			for (int i = 0; i < cmd_length; i++) {
				String k = cmd.substring(i * 2, (i + 1) * 2);
				try {
					outBuffer[i] = (byte) Integer.parseInt(k, 16);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return outBuffer;
	}
	
	public byte[] strtoArray(String cmd) {
		int cmd_length = cmd.length();
		byte[] outBuffer = new byte[cmd_length];

		if (cmd_length > 0) {
			for (int i = 0; i < cmd_length; i++) {
				try {
					outBuffer[i] = (byte) cmd.charAt(i);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return outBuffer;
	}

	public void stxChecker() {
		if (data[0] == 0x15) {
			sendFailMsg("Error: Unknown STX");
		} else if (data[0] == 0x06) {
			count = 0;
		} else if (data[0] == 0x20 && stxCheck == false) {
			mHandler.sendEmptyMessageDelayed(0, 100);
			stxCheck = true;
		} else if (stxCheck == false) {
			sendFailMsg("Error: Unknown Data");
		}
	}

	public void lengthChecker() {
		int dataLength = data[3];
		if (data[dataLength + 5] == 0x03) {
			Analysiser();
		} else {
			sendFailMsg("Error: Data length check failed");
		}
	}

	public void Analysiser() {
		if (data[2] == 0x00) {
			if (currentCmd == "1") {
				/*tv_report.append("Module:");
				tv_report.append(data[4] + "");
				tv_report.append(data[5] + "");
				tv_report.append(data[6] + "");*/
			} else if (currentCmd == "2") {
				if (data[3] != 0x00) {
					byte[] id;
					switch (data[5]) {
					    case 0x04:
					    	id=new byte[]{data[9],data[10],data[11],data[12]};
							ID=bytes2HexStringNoSpace(id, id.length);
					    	break;
					    case 0x44:
					    	id=new byte[]{data[10],data[11],data[12],data[13],data[14],data[15]};
							ID=bytes2HexStringNoSpace(id, id.length);
					    	break;
					    default:
					    	id=new byte[(int)data[3]];
							for(byte i=0;i<data[3];i++)
								id[(int)i]=data[3+((int)i)];
							ID="UNKNOWN CARD : "+bytes2HexStringNoSpace(id, id.length);
					}
					
					if(ReadOnceID)
					{
						if(!AutoRead && !writeID)
						{
			                sendSuccessMsgID();
							StopListening=true;
							closeCmd();
						}
						else
                        {						
						    activateCmd();
						}
					}
					else{
						if(!AutoRead && !writeID)
			                sendSuccessMsgID();
						else					
						    activateCmd();
					}
				}

			} else if (currentCmd == "4") {
				if (data[3] == 0x00) {
					//---- read UID is ON
				} else if (data[3] == 0x19) {
					//---- Unknown
				}

			} else if (currentCmd == "5") {
				if (data[3] == 0x08) {
					String str = "Card : ";
					byte[] id = {data[11],data[10],data[9],data[8]};
					str =bytes2HexString(id, 4)+" is now activated";
					
					if(writeID)
						writeCmd();
				    else
					    readCmd();
				}
			} else if (currentCmd == "6") {
				if (data[4] == -1) {
					//---- Key loaded
				}

			}  else if (currentCmd == "7") {
				if (data[3] == 0x10) {
					byte[] data1 = new byte[16];
					System.arraycopy(data, 4, data1, 0, 16);
					blockData=bytes2String(data1, 16);
					
			        sendSuccessMsgID_DATA();
					
					if(ReadOnceID)
						threadFlag=false;
					else
			            listenCmd();
				}
			}   else if (currentCmd == "8") {
				if (data[4] == -1) {
					newID.remove(0);
					sendSuccessMsg();
					if(newID.length()==0)
					    threadFlag=false;
					else
						listenCmd();
				}
			}

		} else if (currentCmd == "3" && data[2] == 0x01) {
			threadFlag=false;
			if(StopListening)
			    StopListening=false;
		    else
   		        sendSuccessMsg();
		} else {
			if(readID || writeID)
				listenCmd();
			else
		    {
			    threadFlag=false;
			    sendFailMsg("Error occured while executing the command");
			}
		}
		clean();
	}

	public void clean() {
		byte[] cleaner = new byte[64];
		System.arraycopy(cleaner, 0, data, 0, 64);
		stxCheck = false;
		count = 0;
	}
	
	public String bcc(){
		byte[] ary = toArray(et_send);
		int result = (byte) (ary[0] ^ ary[1]);
		for (int i = 2; i < ary.length; i++) {
			result = (byte) (result ^ ary[i]);
		}
		String cmd=Integer.toHexString(~result & 0xFF).toUpperCase() + " 03";
		return cmd;
	}
	
	

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        boolean ok = false;
        writeID=false;
		readID=false;
		ID="";
		
        if (CMD_CANCEL.equals(action)) {
		    cancel(callbackContext);
			
			StartListening();
		    closeCmd();
			
            ok = true;
        } else if (CMD_WRITE.equals(action)) {
		    JSONObject jsonObject=data.optJSONObject(0);
			newID=jsonObject.optJSONArray("data");
			block=jsonObject.optInt("block");
			writeID=true;
			
			write(callbackContext);
			
			boolean exceeded=false;
			for (int i=0; i<newID.length(); i++) {
                String str = newID.optString(i);
				if(str.length()>16)
				{
					exceeded=true;
					break;
				}
            }
			if(exceeded)
				sendFailMsg("Number of characters should not exceed 16.");
			else
			{
			    StartListening();
			    listenCmd();
			}
			
            ok = true;
        } else if (CMD_TURNON.equals(action)) {
			JSONObject jsonObject=data.optJSONObject(0);
			String color=jsonObject.optString("color");
			R=(color.equals("red"))?true:false;
			G=(color.equals("green"))?true:false;
			B=(color.equals("blue"))?true:false;
			
            on(callbackContext);
            ok = true;
        } else if (CMD_TURNOFF.equals(action)) {
            off(callbackContext);
            ok = true;
        } else if (CMD_READ.equals(action)) {
			JSONObject jsonObject=data.optJSONObject(0);
			ReadOnceID=jsonObject.optBoolean("readOnce", false);
			AutoRead=jsonObject.optBoolean("autoReadFromBlock", false);
			block=jsonObject.optInt("block");
			readID=true;
			
            reading(callbackContext);
			
			StartListening();
			listenCmd();
			
            ok = true;
        } else if (CMD_INITPORT.equals(action)) {
		    JSONObject jsonObject=data.optJSONObject(0);
			portName = jsonObject.optString("portName");
		    initPort(callbackContext);
		}
		
        return ok;
    }

    private void listen(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);              
        callback = callbackContext;
    }
    private void StartListening(){
		threadFlag=true;
	}
	
	private void SendCmd(){
		tv_bcc=bcc();
		byte[] outData = toArray(et_send);
		byte[] stx = toArray(et_stx);
		byte[] bcc = toArray(tv_bcc);
		byte[] outBuffer = new byte[4 + outData.length];

		if (stx.length == 2 && bcc.length == 2) {

			System.arraycopy(stx, 0, outBuffer, 0, stx.length);
			System.arraycopy(outData, 0, outBuffer, 2, outData.length);
			System.arraycopy(bcc, 0, outBuffer, outBuffer.length - 2, bcc.length);
 
			try {
				mOutputStream.write(outBuffer, 0, outBuffer.length);
				for (int i = 0; i < outBuffer.length; i++) {
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			//System.out.println("sum error");
		}
	}
	private void closeCmd(){
        clean();
		et_send="81 01 00";
		tv_bcc="";
		currentCmd = "3";
		
		SendCmd();
	}
	private void listenCmd(){
		clean();
		et_send="80 04 07 03 01 00";
		tv_bcc="";
		currentCmd = "2";

        SendCmd();	
	}
	private void activateCmd(){
		clean();
		et_send="10 01 00";
		tv_bcc="";
	    currentCmd = "5";
		
		SendCmd();
	}
	private void readCmd(){
		clean();
		et_send="12 03 00 01 "+(block<10?"0"+block:block);
		tv_bcc="";
	    currentCmd = "7";
		
		SendCmd();
	}
	private void writeCmd(){
		clean();
		byte[] newIDbyte=strtoArray(newID.optString(0));
		
		et_send="11 13 00 01 "+(block<10?"0"+block:block)+" "+bytes2HexString(newIDbyte,newIDbyte.length)+getEmptyHex((16-newIDbyte.length));
		tv_bcc="";
		currentCmd = "8";
				
		SendCmd();
	}
	
    private void sendSuccessMsg(){
			try{
				JSONObject message = new JSONObject();
                message.put("state", "Success");
				
                PluginResult result = new PluginResult(PluginResult.Status.OK, message.toString());
                result.setKeepCallback(true);
                if (callback != null) {
                    callback.sendPluginResult(result);                        
                }
            } catch (Exception e) {
                if (callback != null) {
                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                    result.setKeepCallback(true);
                    callback.sendPluginResult(result);
                }
                e.printStackTrace();
            }	
	}
    private void sendSuccessMsgID_DATA(){
			try{
				JSONObject message = new JSONObject();
                message.put("state", "Success");
			    JSONObject tag = new JSONObject();
			    tag.put("data", blockData);
				tag.put("UID", ID);
			    message.put("tag", tag);
				
                PluginResult result = new PluginResult(PluginResult.Status.OK, message.toString());
                result.setKeepCallback(true);
                if (callback != null) {
                    callback.sendPluginResult(result);                        
                }
            } catch (Exception e) {
                if (callback != null) {
                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                    result.setKeepCallback(true);
                    callback.sendPluginResult(result);
                }
                e.printStackTrace();
            }	
	}
    private void sendSuccessMsgID(){
			try{
				JSONObject message = new JSONObject();
                message.put("state", "Success");
			    JSONObject tag = new JSONObject();
				tag.put("UID", ID);
			    message.put("tag", tag);
				
                PluginResult result = new PluginResult(PluginResult.Status.OK, message.toString());
                result.setKeepCallback(true);
                if (callback != null) {
                    callback.sendPluginResult(result);                        
                }
            } catch (Exception e) {
                if (callback != null) {
                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                    result.setKeepCallback(true);
                    callback.sendPluginResult(result);
                }
                e.printStackTrace();
            }	
	}
    private void sendFailMsg(String msg){
			try{
				JSONObject message = new JSONObject();
                message.put("state", "Failed");
				message.put("msg", msg);
				
                PluginResult result = new PluginResult(PluginResult.Status.OK, message.toString());
                result.setKeepCallback(true);
                if (callback != null) {
                    callback.sendPluginResult(result);                        
                }
            } catch (Exception e) {
                if (callback != null) {
                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                    result.setKeepCallback(true);
                    callback.sendPluginResult(result);
                }
                e.printStackTrace();
            }	
	}
	
    private void write(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);              
        callback = callbackContext;
    }
	
    private void reading(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);              
        callback = callbackContext;
    }
	
    private void on(CallbackContext callbackContext) {
        PluginResult result1 = new PluginResult(PluginResult.Status.NO_RESULT);
        result1.setKeepCallback(true);
        callbackContext.sendPluginResult(result1);              
        callback = callbackContext;
		
		try{
            smdtManager.smdtSetExtrnalGpioValue (1,B?B:false);
            smdtManager.smdtSetExtrnalGpioValue (2,G?G:false);
		}
		catch(Exception e){
            if (callback != null) {
                PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                result.setKeepCallback(true);
                callback.sendPluginResult(result);
            }
            e.printStackTrace();
		}
		
	    sendSuccessMsg();
    }
	
    private void off(CallbackContext callbackContext) {
        PluginResult result1 = new PluginResult(PluginResult.Status.NO_RESULT);
        result1.setKeepCallback(true);
        callbackContext.sendPluginResult(result1);              
        callback = callbackContext;
		
		try{
            smdtManager.smdtSetExtrnalGpioValue (1,true);
            smdtManager.smdtSetExtrnalGpioValue (2,true);
		}
		catch(Exception e){
            if (callback != null) {
                PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                result.setKeepCallback(true);
                callback.sendPluginResult(result);
            }
            e.printStackTrace();
		}
		
		sendSuccessMsg();
    }
	
    private void cancel(CallbackContext callbackContext) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);              
        callback = callbackContext;
    }
	
    private Activity getActivity() {
        return this.cordova.getActivity();
    }

    private Intent getIntent() {
        return getActivity().getIntent();
    }

}
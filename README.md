# Fork of cordova-appivo-acr122

A Cordova plugin that enables reading NFC tags using RFID/NFC Reader for Sibo Tablet.

TODO:

Read/Write data to a block only works for Mifare S50.


API:
- NOTE: Before calling any of the other methods you need to call initPort method with the port name.

- read data from a block :
read(callback,errorCallBack,dataObject)

- cancel the currently registered listener/reader :
cancel()

- write to a block :
write(callback,errorCallBack,dataObject)

- turn on LED :
turnOnLED(dataObject)

- turn off LED :
turnOffLED()


Example:

```javascript
var nfc = window.plugins.NFC;

// Access serial port of the reader
nfc.initPort({
        // portName can be 'ttyS2' or 'ttyS3' or any other name based on your model
        portName: 'ttyS2'
    }).then(
    function(result) {
        alert(JSON.stringify(result));
    },
    function (error) {
        alert(error);
    }
);

// Write data onto a tags block
nfc.write(function(event) {
    if (event.state == 'Success') {
        alert("Data written Successfully!!!");
    }
},
function(error){
    alert(error);
},
{
    // provide the data to be written onto the tag. If less than 16 characters 0's will be added at the end.
    // 'block' is the block number onto which the data will be written
    data:["newID"],
    block:5
});

// read UID and data from a tags block
nfc.read(function(event) {
    if (event.state == 'Success') {
        alert("Scanned tag with data " + event.tag.data+"\n"+
		      "Scanned tag with UID " + event.tag.UID);
    }
},
function(error){
    alert(error);
},
{
    // If readOnce is false it will keep the listener running until 'cancel' is called
    // If autoReadFromBlock is true it will read data from block as well
    // block is block number to read
    readOnce:false,
    autoReadFromBlock:true,
    block:5
});

// Stop reading tags
nfc.cancel().then(
    function(result) {
        alert(JSON.stringify(result));
    },
    function (error) {
        alert(error);
    }
);

// Turn on LED
nfc.ledOn({
        // color can be 'red', 'green' or 'blue'
        color:red
    }).then(
    function(result) {
        alert(JSON.stringify(result));
    },
    function (error) {
        alert(error);
    }
);

// Turn off LED
nfc.ledOff().then(
    function(result) {
        alert(JSON.stringify(result));
    },
    function (error) {
        alert(error);
    }
);
```

NOTE:

If you get configuration errors: Use oldest version OR Update "resource-file" and "lib-file" tags in plugin.xml file of the latest version to match those of oldest version OR Update your cordova to the latest version.

Use @1.2.8 or later for both 32 and 64 bit library.
